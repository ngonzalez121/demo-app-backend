#### Rails Demo App


##### Install Ruby
```shell
rbenv local 3.3.6
```

##### Run bundle install

```shell
bundle install
```

##### Create database
```shell
bundle exec rails db:create db:migrate
```

##### Run rspec
```shell
RAILS_ENV=test bundle exec rspec
```
