ActionDispatch::Request.parameter_parsers[:json] = -> raw_post {
  begin
    JSON.parse(
      ActiveSupport::Gzip.decompress(raw_post)
    )
  rescue Zlib::GzipFile::Error => e
    JSON.parse(raw_post)
  end
}
