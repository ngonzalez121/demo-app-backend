require 'active_support'
require 'active_support/core_ext'
require 'dotenv/load'
require 'yaml'

# APP_HOSTNAME
if ENV['APP_HOSTNAME'].present?
  APP_HOSTNAME = ENV['APP_HOSTNAME']
else
  raise "Missing ENV APP_HOSTNAME"
end

# APP_PORT
if ENV['APP_PORT'].present?
  APP_PORT = ENV['APP_PORT']
else
  raise "Missing ENV APP_PORT"
end

# NGINX_PORT
if ENV['NGINX_PORT'].present?
  NGINX_PORT = ENV['NGINX_PORT']
else
  raise "Missing ENV NGINX_PORT"
end

# AZURE_ACCOUNT_NAME
if ENV['AZURE_ACCOUNT_NAME'].present?
  AZURE_ACCOUNT_NAME = ENV['AZURE_ACCOUNT_NAME']
else
  raise "Missing ENV AZURE_ACCOUNT_NAME"
end

# AZURE_CONTAINER_NAME
if ENV['AZURE_CONTAINER_NAME'].present?
  AZURE_CONTAINER_NAME = ENV['AZURE_CONTAINER_NAME']
else
  raise "Missing ENV AZURE_CONTAINER_NAME"
end

# AZURE_ACCESS_KEY
if ENV['AZURE_ACCESS_KEY'].present?
  AZURE_ACCESS_KEY = ENV['AZURE_ACCESS_KEY']
else
  raise "Missing ENV AZURE_ACCESS_KEY"
end

# POSTGRESQL_HOST
if ENV['POSTGRESQL_HOST'].present?
  POSTGRESQL_HOST = ENV['POSTGRESQL_HOST']
else
  raise "Missing ENV POSTGRESQL_HOST"
end

# POSTGRESQL_PORT
if ENV['POSTGRESQL_PORT'].present?
  POSTGRESQL_PORT = ENV['POSTGRESQL_PORT']
else
  raise "Missing ENV POSTGRESQL_PORT"
end

# POSTGRESQL_DB
if ENV['POSTGRESQL_DB'].present?
  POSTGRESQL_DB = ENV['POSTGRESQL_DB']
else
  raise "Missing ENV POSTGRESQL_DB"
end

# POSTGRESQL_USERNAME
if ENV['POSTGRESQL_USERNAME'].present?
  POSTGRESQL_USERNAME = ENV['POSTGRESQL_USERNAME']
else
  raise "Missing ENV POSTGRESQL_USERNAME"
end

# POSTGRESQL_PASSWORD
if ENV['POSTGRESQL_PASSWORD'].present?
  POSTGRESQL_PASSWORD = ENV['POSTGRESQL_PASSWORD']
else
  raise "Missing ENV POSTGRESQL_PASSWORD"
end

# REDIS_HOST
if ENV['REDIS_HOST'].present?
  REDIS_HOST = ENV['REDIS_HOST']
else
  raise "Missing ENV REDIS_HOST"
end

# REDIS_PORT
if ENV['REDIS_PORT'].present?
  REDIS_PORT = ENV['REDIS_PORT']
else
  raise "Missing ENV REDIS_PORT"
end

# REDIS_DB
if ENV['REDIS_DB'].present?
  REDIS_DB = ENV['REDIS_DB']
else
  raise "Missing ENV REDIS_DB"
end

# SECRET_KEY_BASE
if ENV['SECRET_KEY_BASE'].present?
  SECRET_KEY_BASE = ENV['SECRET_KEY_BASE']
else
  raise "Missing ENV SECRET_KEY_BASE"
end

# Settings
ALLOWED_VIDEO_FORMATS   = YAML.load_file File.expand_path('../config/yaml/allowed_video_formats.yaml', __dir__)
ALLOWED_AUDIO_FORMATS   = YAML.load_file File.expand_path('../config/yaml/allowed_audio_formats.yaml', __dir__)
ALLOWED_IMAGE_FORMATS   = YAML.load_file File.expand_path('../config/yaml/allowed_image_formats.yaml', __dir__)
ALLOWED_PDF_FORMATS     = YAML.load_file File.expand_path('../config/yaml/allowed_pdf_formats.yaml', __dir__)
ALLOWED_TEXT_FORMATS    = YAML.load_file File.expand_path('../config/yaml/allowed_text_formats.yaml', __dir__)
STOP_WORDS              = YAML.load_file File.expand_path('../config/yaml/stop_words.yaml', __dir__)

