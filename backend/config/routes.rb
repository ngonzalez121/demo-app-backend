Rails.application.routes.draw do
  # root
  get "home/index"
  root to: "home#index"

  # graphql
  post "/graphql", to: "graphql#execute"

  # uploads
  post "/upload" => "uploads#create"
  get "/upload" => "uploads#index"

  # Authentication
  post "/session" => "sessions#create"
  delete "/session" => "sessions#destroy"
  post "/registration" => "registrations#create"
  post "/password" => "passwords#create"

  # health
  get "/_health" => "health_check#check"
end
