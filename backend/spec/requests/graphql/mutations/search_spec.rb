require "rails_helper"

RSpec.describe "Search folders" do
  subject(:request) {
    post "/graphql", headers: { 'Content-Type' => 'application/json' }, params: { query: query, variables: variables }.to_json
  }
  let(:folder) { FactoryBot.create(:folder) }
  let(:query) do
    <<~GRAPHQL
    mutation search($q: String!, $page: Int!, $folder: String, $subfolder: String) {
      search(input: { q: $q, page: $page, folder: $folder, subfolder: $subfolder }) {
        results {
          attributes
        }
        loading
        errors
        currentPage
        totalPages
      }
    }
    GRAPHQL
  end
  let(:variables) do
    {
      q: folder.formatted_name,
      folder: '',
      subfolder: '',
      page: 1,
    }
  end
  let(:expected_result) do
    {
      data: {
        search: {
          results: [{
            attributes: {
              dataUrl: folder.data_url,
              name: folder.formatted_name,
              folder: folder.folder,
              subfolder: folder.subfolder,
            }
          }],
          loading: false,
          errors: [],
          currentPage: 1,
          totalPages: 1,
        },
      }
    }
  end

  it "returns a 200 HTTP code" do
    request
    expect(response).to have_http_status 200
  end

  it "returns search results" do
    request
    expect(JSON.parse(response.body).deep_symbolize_keys).to include_json expected_result
  end
end
