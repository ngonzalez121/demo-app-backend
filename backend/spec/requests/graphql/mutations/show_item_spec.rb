require "rails_helper"

RSpec.describe "Display folder" do
  subject(:request) {
    post "/graphql", headers: { 'Content-Type' => 'application/json' }, params: { query: query, variables: variables }.to_json
  }
  let(:folder) { FactoryBot.create(:folder) }
  let(:query) do
    <<~GRAPHQL
    mutation showItem(
      $id: String!,
    ) {
      showItem(input: {
        id: $id,
      }) {
        folder {
          attributes
        }
        audioFiles {
          attributes
        }
        imageFiles {
          attributes
        }
        pdfFiles {
          attributes
        }
        textFiles {
          attributes
        }
        loading
        errors
      }
    }
    GRAPHQL
  end
  let(:variables) do
    {
      id: folder.data_url,
    }
  end
  let(:expected_result) do
    {
      data: {
        showItem: {
          folder: {
            attributes: {
              dataUrl: folder.data_url,
              name: folder.formatted_name,
              folder: folder.folder,
              subfolder: folder.subfolder,
            }
          },
          audioFiles: [],
          imageFiles: [],
          pdfFiles: [],
          textFiles: [],
          loading: false,
          errors: [],
        },
      }
    }
  end

  it "returns a 200 HTTP code" do
    request
    expect(response).to have_http_status 200
  end

  it "returns search results" do
    request
    expect(JSON.parse(response.body).deep_symbolize_keys).to include_json expected_result
  end
end
