FactoryBot.define do
  factory :folder do
    name { Faker::Name.name }
    formatted_name { name }
    folder { 'import_folder' }
    subfolder { 'import_subfolder' }
    data_url { name.gsub(' ', '-') }
    upload_uuid { SecureRandom.uuid }
  end
end
