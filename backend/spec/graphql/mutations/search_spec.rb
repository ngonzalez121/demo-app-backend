require "rails_helper"

RSpec.describe Mutations::Search do
  context "Mutations" do
    let(:folder) { FactoryBot.create(:folder) }
    it "Search folders" do
      variables = {
        q: folder.formatted_name,
        folder: '',
        subfolder: '',
        page: 1,
      }

      query = GraphQL::Query.new(
        BackendSchema,
        mutation,
        variables: variables.deep_stringify_keys,
        context: {},
      )

      result = query.result

      expect(result.dig("data", "search", "results", 0, "attributes", "dataUrl")).to eq(folder.data_url)
      expect(result.dig("data", "search", "results", 0, "attributes", "name")).to eq(folder.formatted_name)
      expect(result.dig("data", "search", "results", 0, "attributes", "folder")).to eq(folder.folder)
      expect(result.dig("data", "search", "results", 0, "attributes", "subfolder")).to eq(folder.subfolder)
    end
  end

  def mutation
    <<~GQL
    mutation search(
      $q: String!,
      $page: Int!,
      $folder: String!,
      $subfolder: String!,
    ) {
      search(input: {
        q: $q,
        page: $page,
        folder: $folder,
        subfolder: $subfolder,
      }) {
        results {
          attributes
        }
        loading
        errors
        currentPage
        totalPages
      }
    }
    GQL
  end
end
