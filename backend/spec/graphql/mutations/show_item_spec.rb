require "rails_helper"

RSpec.describe Mutations::ShowItem do
  context "Mutations" do
    let(:folder) { FactoryBot.create(:folder) }
    it "Display folder" do
      variables = {
        id: folder.data_url,
      }

      query = GraphQL::Query.new(
        BackendSchema,
        mutation,
        variables: variables.deep_stringify_keys,
        context: {},
      )

      result = query.result

      expect(result.dig("data", "showItem", "folder", "attributes", "dataUrl")).to eq(folder.data_url)
      expect(result.dig("data", "showItem", "folder", "attributes", "name")).to eq(folder.formatted_name)
      expect(result.dig("data", "showItem", "folder", "attributes", "folder")).to eq(folder.folder)
      expect(result.dig("data", "showItem", "folder", "attributes", "subfolder")).to eq(folder.subfolder)
      expect(result.dig("data", "showItem", "audioFiles")).to eq([])
      expect(result.dig("data", "showItem", "imageFiles")).to eq([])
      expect(result.dig("data", "showItem", "pdfFiles")).to eq([])
      expect(result.dig("data", "showItem", "textFiles")).to eq([])
    end
  end

  def mutation
    <<~GQL
    mutation showItem(
      $id: String!,
    ) {
      showItem(input: {
        id: $id,
      }) {
        folder {
          attributes
        }
        audioFiles {
          attributes
        }
        imageFiles {
          attributes
        }
        pdfFiles {
          attributes
        }
        textFiles {
          attributes
        }
        loading
        errors
        loading
        errors
      }
    }
    GQL
  end
end
