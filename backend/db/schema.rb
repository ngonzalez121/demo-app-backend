# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[8.0].define(version: 2024_12_15_111841) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pg_catalog.plpgsql"
  enable_extension "unaccent"

  # Custom types defined in this database.
  # Note that some types may not work with other database engines. Be careful if changing database.
  create_enum "folder_aasm_states", ["created", "published", "archived"]

  create_table "attachments", force: :cascade do |t|
    t.bigint "folder_id", null: false
    t.string "file_uid", null: false
    t.string "file_name", null: false
    t.datetime "file_created_at", null: false
    t.datetime "file_updated_at", null: false
    t.uuid "upload_uuid", null: false
    t.string "type", null: false
    t.string "formatted_name"
    t.string "data_url"
    t.json "parameters"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.index ["folder_id", "data_url"], name: "index_attachments_on_folder_id_and_data_url", unique: true
    t.index ["folder_id", "file_name"], name: "index_attachments_on_folder_id_and_file_name", unique: true
    t.index ["folder_id", "file_uid"], name: "index_attachments_on_folder_id_and_file_uid", unique: true
    t.index ["folder_id"], name: "index_attachments_on_folder_id"
  end

  create_table "folders", force: :cascade do |t|
    t.string "name", null: false
    t.string "folder"
    t.string "subfolder"
    t.uuid "upload_uuid", null: false
    t.enum "aasm_state", default: "created", null: false, enum_type: "folder_aasm_states"
    t.string "formatted_name"
    t.string "data_url"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.index ["data_url"], name: "index_folders_on_data_url", unique: true
    t.index ["upload_uuid", "name", "folder", "subfolder"], name: "index_folders_on_upload_uuid_name_folder_subfolder", unique: true
  end

  create_table "images", force: :cascade do |t|
    t.bigint "folder_id", null: false
    t.string "file_uid", null: false
    t.string "thumb_uid", null: false
    t.string "file_name", null: false
    t.datetime "file_created_at", null: false
    t.datetime "file_updated_at", null: false
    t.uuid "upload_uuid", null: false
    t.string "type", null: false
    t.string "formatted_name"
    t.string "data_url"
    t.json "parameters"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.index ["folder_id", "data_url"], name: "index_images_on_folder_id_and_data_url", unique: true
    t.index ["folder_id", "file_name"], name: "index_images_on_folder_id_and_file_name", unique: true
    t.index ["folder_id", "file_uid"], name: "index_images_on_folder_id_and_file_uid", unique: true
    t.index ["folder_id"], name: "index_images_on_folder_id"
  end

  create_table "sessions", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.string "ip_address"
    t.string "user_agent"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_sessions_on_user_id"
  end

  create_table "uploads", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.uuid "uuid", null: false
    t.string "file_path", null: false
    t.string "mime_type", null: false
    t.datetime "file_created_at", null: false
    t.datetime "file_updated_at", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.index ["user_id", "file_path"], name: "index_uploads_on_user_id_and_file_path", unique: true
    t.index ["user_id"], name: "index_uploads_on_user_id"
    t.index ["uuid"], name: "index_uploads_on_uuid", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.uuid "uuid", null: false
    t.string "first_name"
    t.string "last_name"
    t.string "email_address", null: false
    t.string "password_digest", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.datetime "deleted_at"
    t.index ["email_address"], name: "index_users_on_email_address", unique: true
  end

  create_table "versions", force: :cascade do |t|
    t.string "item_type", null: false
    t.integer "item_id", null: false
    t.string "event", null: false
    t.string "whodunnit"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.json "object"
    t.json "object_changes"
    t.index ["item_type", "item_id"], name: "index_versions_on_item_type_and_item_id"
  end

  add_foreign_key "attachments", "folders"
  add_foreign_key "images", "folders"
  add_foreign_key "sessions", "users"
  add_foreign_key "uploads", "users"
end
