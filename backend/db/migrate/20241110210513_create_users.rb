class CreateUsers < ActiveRecord::Migration[8.0]
  def change
    create_table :users do |t|
      t.uuid :uuid, null: false
      t.string :first_name
      t.string :last_name
      t.string :email_address, null: false
      t.string :password_digest, null: false
      t.datetime :created_at
      t.datetime :updated_at
      t.datetime :deleted_at
      t.index [:email_address], name: :index_users_on_email_address, unique: true
    end
  end
end
