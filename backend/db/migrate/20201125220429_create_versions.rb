class CreateVersions < ActiveRecord::Migration[8.0]
  def change
    create_table :versions do |t|
      t.string :item_type, null: false
      t.integer :item_id, null: false
      t.string :event, null: false
      t.string :whodunnit
      t.datetime :created_at
      t.datetime :updated_at
      t.json :object          # Full object changes
      t.json :object_changes  # Optional column-level changes
      t.index [:item_type, :item_id], name: :index_versions_on_item_type_and_item_id
    end
  end
end
