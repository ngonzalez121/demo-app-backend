class CreateUploads < ActiveRecord::Migration[8.0]
  def change
    create_table :uploads do |t|
      t.references :user, null: false, foreign_key: true
      t.uuid :uuid, null: false
      t.string :file_path, null: false
      t.string :mime_type, null: false
      t.datetime :file_created_at, null: false
      t.datetime :file_updated_at, null: false
      t.datetime :created_at
      t.datetime :updated_at
      t.datetime :deleted_at
      t.index [:uuid], name: :index_uploads_on_uuid, unique: true
      t.index [:user_id, :file_path], name: :index_uploads_on_user_id_and_file_path, unique: true
    end
  end
end
