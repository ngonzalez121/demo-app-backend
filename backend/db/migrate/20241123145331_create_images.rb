class CreateImages < ActiveRecord::Migration[8.0]
  def change
    create_table :images do |t|
      t.references :folder, null: false, foreign_key: true
      t.string :file_uid, null: false
      t.string :thumb_uid, null: false
      t.string :file_name, null: false
      t.datetime :file_created_at, null: false
      t.datetime :file_updated_at, null: false
      t.uuid :upload_uuid, null: false
      t.string :type, null: false
      t.string :formatted_name
      t.string :data_url
      t.json :parameters
      t.datetime :created_at
      t.datetime :updated_at
      t.datetime :deleted_at
      t.index [:folder_id, :file_uid], name: :index_images_on_folder_id_and_file_uid, unique: true
      t.index [:folder_id, :file_name], name: :index_images_on_folder_id_and_file_name, unique: true
      t.index [:folder_id, :data_url], name: :index_images_on_folder_id_and_data_url, unique: true
    end
  end
end
