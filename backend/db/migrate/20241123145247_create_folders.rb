class CreateFolders < ActiveRecord::Migration[8.0]
  def change
    execute "CREATE TYPE folder_aasm_states AS ENUM ('%s');" % [:created, :published, :archived].join('\', \'')
    create_table :folders do |t|
      t.string :name, null: false
      t.string :folder
      t.string :subfolder
      t.uuid :upload_uuid, null: false
      t.column :aasm_state, "folder_aasm_states", null: false, default: "created"
      t.string :formatted_name
      t.string :data_url
      t.datetime :created_at
      t.datetime :updated_at
      t.datetime :deleted_at
      t.index [:upload_uuid, :name, :folder, :subfolder], name: :index_folders_on_upload_uuid_name_folder_subfolder, unique: true
      t.index [:data_url], name: :index_folders_on_data_url, unique: true
    end
  end
end
