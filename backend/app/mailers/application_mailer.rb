class ApplicationMailer < ActionMailer::Base
  default from: "DoNotReply@link12.ddns.net"
  layout "mailer"
end
