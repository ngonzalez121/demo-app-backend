# == Schema Information
#
# Table name: attachments
#
#  id              :integer          not null, primary key
#  folder_id       :integer          not null
#  file_uid        :string           not null
#  file_name       :string           not null
#  file_created_at :datetime         not null
#  file_updated_at :datetime         not null
#  upload_uuid     :uuid             not null
#  type            :string           not null
#  formatted_name  :string
#  data_url        :string
#  parameters      :json
#  created_at      :datetime
#  updated_at      :datetime
#  deleted_at      :datetime
#
# Indexes
#
#  index_attachments_on_folder_id                (folder_id)
#  index_attachments_on_folder_id_and_data_url   (folder_id,data_url) UNIQUE
#  index_attachments_on_folder_id_and_file_name  (folder_id,file_name) UNIQUE
#  index_attachments_on_folder_id_and_file_uid   (folder_id,file_uid) UNIQUE
#

class Text < Attachment
  dragonfly_accessor :file do
    storage_options {|a| { path: "text/%s" % [ SecureRandom.uuid ] } }
  end
end
