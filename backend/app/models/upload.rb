# == Schema Information
#
# Table name: uploads
#
#  id              :integer          not null, primary key
#  user_id         :integer          not null
#  uuid            :uuid             not null
#  file_path       :string           not null
#  file_created_at :datetime         not null
#  file_updated_at :datetime         not null
#  mime_type       :string           not null
#  created_at      :datetime
#  updated_at      :datetime
#  deleted_at      :datetime
#
# Indexes
#
#  index_uploads_on_user_id                (user_id)
#  index_uploads_on_user_id_and_file_path  (user_id,file_path) UNIQUE
#  index_uploads_on_uuid                   (uuid) UNIQUE
#

class Upload < ActiveRecord::Base
  belongs_to :user, foreign_key: :user_id

  validates_presence_of :user_id
  validates_presence_of :uuid
  validates_presence_of :file_path
  validates_presence_of :mime_type
  validates_presence_of :file_created_at
  validates_presence_of :file_updated_at

  has_paper_trail

  acts_as_paranoid

  def images
    Image.where :upload_uuid => uuid
  end

  def pdfs
    Pdf.where :upload_uuid => uuid
  end

  def texts
    Text.where :upload_uuid => uuid
  end

  def audio_files
    AudioFile.where :upload_uuid => uuid
  end

  def video_files
    VideoFile.where :upload_uuid => uuid
  end

  def to_builder
    Jbuilder.new do |json|
      json.id id if persisted?
      json.uuid uuid
      json.imageFiles do
        json.array! images.decorate do |image|
          json.id image.id
          json.folder image.folder.to_builder.attributes!
          json.thumbUrl image.thumb_url
          json.dataUrl image.data_url
          json.fileName image.file_name
          json.fileUrl image.backend_file_url
          json.mimeType image.parameters["mimeType"]
          json.formatInfo image.parameters["formatInfo"]
          json.fileSize image.file_info(key: "FileSize", type: String)
          json.width image.file_info(key: "ImageWidth", type: Integer)
          json.height image.file_info(key: "ImageHeight", type: Integer)
          json.dimensions image.file_info(key: "ImageSize", type: String)
          json.megapixels image.file_info(key: "Megapixels", type: Float)
        end
      end

      json.pdfFiles do
        json.array! pdfs do |pdf|
          json.id pdf.id
          json.folder pdf.folder.to_builder.attributes!
          json.dataUrl pdf.data_url
          json.fileName pdf.file_name
          json.fileUrl pdf.backend_file_url
          json.mimeType pdf.parameters["mimeType"]
          json.formatInfo pdf.parameters["formatInfo"]
        end
      end

      json.textFiles do
        json.array! texts do |text|
          json.id text.id
          json.folder text.folder.to_builder.attributes!
          json.dataUrl text.data_url
          json.fileName text.file_name
          json.fileUrl text.backend_file_url
          json.mimeType text.parameters["mimeType"]
          json.formatInfo text.parameters["formatInfo"]
        end
      end

      json.audioFiles do
        json.array! audio_files.decorate do |audio_file|
          json.id audio_file.id
          json.folder audio_file.folder.to_builder.attributes!
          json.dataUrl audio_file.data_url
          json.fileName audio_file.file_name
          json.fileUrl audio_file.backend_file_url
          json.mimeType audio_file.parameters["mimeType"]
          json.formatInfo audio_file.parameters["formatInfo"]
          json.length audio_file.track_info(key: "Duration", track: "Audio", type: Float)
          json.bitrate audio_file.track_info(key: "BitRate", track: "Audio", type: Integer)
          json.channels audio_file.track_info(key: "Channels", track: "Audio", type: Integer)
          json.sampleRate audio_file.track_info(key: "SamplingRate", track: "Audio", type: Integer)
          json.fileSize audio_file.track_info(key: "FileSize", track: "General", type: Integer)
        end
      end

      json.videoFiles do
        json.array! video_files.decorate do |video_file|
          json.id video_file.id
          json.folder video_file.folder.to_builder.attributes!
          json.dataUrl video_file.data_url
          json.fileName video_file.file_name
          json.fileUrl video_file.backend_file_url
          json.mimeType video_file.parameters["mimeType"]
          json.formatInfo video_file.parameters["formatInfo"]
          json.length video_file.track_info(key: "Duration", track: "Video", type: Float)
          json.bitrate video_file.track_info(key: "BitRate", track: "Video", type: Integer)
          json.frameRate video_file.track_info(key: "FrameRate", track: "Video", type: Integer)
          json.width video_file.track_info(key: "Width", track: "Video", type: Integer)
          json.height video_file.track_info(key: "Height", track: "Video", type: Integer)
          json.aspectRatio video_file.track_info(key: "DisplayAspectRatio", track: "Video", type: Integer)
          json.fileSize video_file.track_info(key: "FileSize", track: "General", type: Integer)
        end
      end
    end
  end
end
