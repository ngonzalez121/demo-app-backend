# == Schema Information
#
# Table name: folders
#
#  id             :integer          not null, primary key
#  name           :string           not null
#  folder         :string
#  subfolder      :string
#  upload_uuid    :uuid             not null
#  formatted_name :string
#  data_url       :string
#  created_at     :datetime
#  updated_at     :datetime
#  deleted_at     :datetime
#
# Indexes
#
#  index_folders_on_data_url                           (data_url) UNIQUE
#  index_folders_on_upload_uuid_name_folder_subfolder  (upload_uuid,name,folder,subfolder) UNIQUE
#

class Folder < ActiveRecord::Base
  include AASM

  include FolderConcern

  self.table_name = :folders

  has_many :audio_files, class_name: "AudioFile", dependent: :destroy, foreign_key: :folder_id

  has_many :video_files, class_name: "VideoFile", dependent: :destroy, foreign_key: :folder_id

  has_many :images, class_name: "Image", dependent: :destroy, foreign_key: :folder_id

  has_many :pdfs, class_name: "Pdf", dependent: :destroy, foreign_key: :folder_id

  has_many :texts, class_name: "Text", dependent: :destroy, foreign_key: :folder_id

  searchable :formatted_name, :folder, :subfolder

  has_paper_trail

  acts_as_paranoid

  aasm do
    state :created, initial: true
    state :published, :archived

    event :publish do
      transitions from: [:created], to: :published
    end

    event :unpublish do
      transitions from: [:published], to: :created
    end

    event :archive do
      transitions from: [:created, :published], to: :archived
    end

    event :unarchive do
      transitions from: [:archived], to: :created
    end
  end

  def to_builder
    Jbuilder.new do |json|
      json.id id if persisted?
      json.name formatted_name
      json.state aasm_state
      json.dataUrl data_url
      json.folder folder if folder
      json.subfolder subfolder if subfolder
      json.errors errors.full_messages if errors.any?
      json.createdAt created_at if persisted?
      json.updatedAt updated_at if persisted?
    end
  end
end
