# == Schema Information
#
# Table name: images
#
#  id              :integer          not null, primary key
#  folder_id       :integer          not null
#  file_uid        :string           not null
#  thumb_uid       :string           not null
#  file_name       :string           not null
#  file_created_at :datetime         not null
#  file_updated_at :datetime         not null
#  upload_uuid     :uuid             not null
#  type            :string           not null
#  formatted_name  :string
#  data_url        :string
#  parameters      :json
#  created_at      :datetime
#  updated_at      :datetime
#  deleted_at      :datetime
#
# Indexes
#
#  index_images_on_folder_id                (folder_id)
#  index_images_on_folder_id_and_data_url   (folder_id,data_url) UNIQUE
#  index_images_on_folder_id_and_file_name  (folder_id,file_name) UNIQUE
#  index_images_on_folder_id_and_file_uid   (folder_id,file_uid) UNIQUE
#

class Image < Attachment
  self.table_name = :images
  
  dragonfly_accessor :file do
    storage_options {|a| { path: "img/%s" % [ SecureRandom.uuid ] } }
    copy_to(:thumb){|a| a.thumb("600x500>") }
  end

  dragonfly_accessor :thumb do
    storage_options {|a| { path: "thumb/%s" % [ SecureRandom.uuid ] } }
  end

  def thumb_url
    return unless thumb
    [backend_server_url, thumb.url].join ""
  end

  def thumb_remote_url
    return unless thumb
    thumb.remote_url(scheme: 'https')
  end

  def to_builder
    Jbuilder.new do |json|
      json.merge! super
      json.thumbUrl thumb_url
    end
  end
end
