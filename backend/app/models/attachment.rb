# == Schema Information
#
# Table name: attachments
#
#  id              :integer          not null, primary key
#  folder_id       :integer          not null
#  file_uid        :string           not null
#  file_name       :string           not null
#  file_created_at :datetime         not null
#  file_updated_at :datetime         not null
#  upload_uuid     :uuid             not null
#  type            :string           not null
#  formatted_name  :string
#  data_url        :string
#  parameters      :json
#  created_at      :datetime
#  updated_at      :datetime
#  deleted_at      :datetime
#
# Indexes
#
#  index_attachments_on_folder_id                (folder_id)
#  index_attachments_on_folder_id_and_data_url   (folder_id,data_url) UNIQUE
#  index_attachments_on_folder_id_and_file_name  (folder_id,file_name) UNIQUE
#  index_attachments_on_folder_id_and_file_uid   (folder_id,file_uid) UNIQUE
#

class Attachment < ActiveRecord::Base

  self.table_name = :attachments

  self.inheritance_column = :type

  belongs_to :folder, class_name: 'Folder', foreign_key: :folder_id

  include AttachmentConcern

  searchable :parameters

  has_paper_trail

  acts_as_paranoid

  store :parameters, coder: JSON

  def backend_file_url
    return unless file
    [backend_server_url, file.url].join('')
  end

  def file_remote_url
    return unless file
    file.remote_url(scheme: 'https')
  end

  def to_builder
    Jbuilder.new do |json|
      json.id id if persisted?
      json.dataUrl data_url
      json.fileUrl backend_file_url
      json.fileName file_name
      json.fileCreatedAt file_created_at
      json.fileUpdatedAt file_updated_at
      json.createdAt created_at if persisted?
      json.updatedAt updated_at if persisted?
      json.parameters parameters if persisted?
      json.errors do
        json.merge! errors.full_messages
      end
    end
  end

  private

  def backend_server_url
    "https://#{APP_HOSTNAME}:#{NGINX_PORT}"
  end
end
