# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  uuid            :uuid             not null
#  first_name      :string
#  last_name       :string
#  email_address   :string           not null
#  password_digest :string           not null
#  created_at      :datetime
#  updated_at      :datetime
#  deleted_at      :datetime
#
# Indexes
#
#  index_users_on_email_address  (email_address) UNIQUE
#

class User < ApplicationRecord
  has_secure_password

  has_many :uploads, dependent: :destroy

  has_many :sessions, dependent: :destroy

  normalizes :email_address, with: ->(e) { e.strip.downcase }

  validates :first_name, presence: true

  validates :last_name, presence: true

  validates :email_address, uniqueness: { case_sensitive: false }, presence: true

  validates :email_address, format: { with: Authentication::EMAIL_REGEX }

  searchable :email_address, :first_name, :last_name

  has_paper_trail

  acts_as_paranoid

  def to_builder
    Jbuilder.new do |json|
      json.id id if persisted?
      json.uuid uuid
      json.firstName first_name
      json.lastName last_name
      json.emailAddress email_address
      json.errors errors.full_messages
      json.createdAt created_at if persisted?
      json.updatedAt updated_at if persisted?
    end
  end
end
