module FolderConcern
  extend ActiveSupport::Concern

  included do
    before_save do
      if name_changed?
        self.formatted_name = get_formatted_name(name)
        self.data_url = get_data_url
      end
    end
    # any code that you want inside the class
    # that includes this concern
    private
    def get_data_url
      data_url = formatted_name.downcase
      data_url = data_url.gsub(' ', '-')
      data_url = data_url.gsub('_', '-')
      data_url = data_url.gsub('__', '-')
      data_url = data_url.gsub('--', '-')
      data_url = data_url.gsub(/[^\_\-0-9a-z ]/i, '')
      data_url
    end
    def get_formatted_name(name)
      formatted_name_str = if name =~ /\d{4}/
        name[0..((name =~ /\d{4}/) -1)]
      else
        name
      end
      STOP_WORDS.each { |stopword|
        formatted_name_str = formatted_name_str.gsub(/#{stopword}/, '')
      }
      formatted_name_str = formatted_name_str.gsub('_-_', '-')
      formatted_name_str = formatted_name_str.gsub('.', '')
      formatted_name_str = formatted_name_str.gsub('-', ' ')
      formatted_name_str = formatted_name_str.gsub('_', ' ')
      formatted_name_str = formatted_name_str.gsub('()', '')
      formatted_name_str = formatted_name_str.gsub('--', '')
      formatted_name_str = formatted_name_str.gsub(/[^\_\-0-9a-z ]/i, '')
      formatted_name_str = formatted_name_str.split.join ' '
      formatted_name_str = formatted_name_str.capitalize
    end
  end

  class_methods do
    # methods that you want to create as
    # class methods on the including class
  end
end
