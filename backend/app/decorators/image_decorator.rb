class ImageDecorator < Draper::Decorator
  delegate_all

  def file_info(key:nil, type:nil)
    return unless object.parameters.key?('exiftool')

    value = nil
    object.parameters['exiftool'].each do |file_infos|
      if file_infos.has_key?(key)
        value = file_infos[key]
        break
      end
    end
    if type == Float
      value.to_f
    elsif type == Integer
      value.to_i
    elsif type == String
      value.to_s
    end
  end
end
