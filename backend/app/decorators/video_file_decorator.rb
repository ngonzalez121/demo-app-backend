class VideoFileDecorator < Draper::Decorator
  delegate_all

  def track_info(key:nil, track:nil, type:nil)
    return unless object.parameters.key?('mediainfo')

    value = nil
    object.parameters['mediainfo']['media']['track'].each do |media_track|
      if media_track['@type'] == track && media_track.has_key?(key)
        value = media_track[key]
        break
      end
    end
    if type == Float
      value.to_f
    elsif type == Integer
      value.to_i
    elsif type == String
      value.to_s
    end
  end
end
