class UploadWorker
  include Sidekiq::Worker
  sidekiq_options :queue => :upload,
                  :backtrace => true,
                  :retry => false

  attr_accessor :folder_infos
  attr_accessor :folder
  attr_accessor :params
  attr_accessor :item
  attr_accessor :file
  attr_accessor :upload

  def perform(request_body)
    @params = JSON.parse(request_body)
    set_folder_infos
    create_folder
    set_file
    write_file
    create_item
  rescue => exception
    Rails.logger.error(exception)
  end

  private

  def set_file
    @file ||= Tempfile.new
  end

  def write_file
    file.binmode
    file.write(decode_data)
    file.rewind
  end

  def decode_data
    Base64.decode64(params['itemData'])
  end

  def create_item
    @item = if text_file?
      create_text_file
    elsif image_file?
      create_image_file
    elsif audio_file?
      create_audio_file
    elsif video_file?
      create_video_file
    elsif pdf_file?
      create_pdf_file
    end

    # set item parameters
    if item
      set_file_mime_type
      set_format_info
      set_image_file_metadata if image_file?
      set_audio_video_file_metadata if audio_file? || video_file?
      item.save!
    end

    # create m3u8 playlist
    if item.persisted?
      if audio_file?
        HTTParty.get("https://#{NGINX_HOST}:#{NGINX_PORT}/audio_files/#{item.id}")
      end

      if video_file?
        HTTParty.get("http://#{NGINX_HOST}:#{NGINX_PORT}/video_files/#{item.id}")
      end
    end
  end

  def set_file_mime_type
    item.parameters['mimeType'] = params['mimeType']
  end

  def set_audio_video_file_metadata
    item.parameters['mediainfo'] = JSON.parse(`mediainfo --output=JSON #{Shellwords.escape(item.file.path)}`)
  end

  def set_image_file_metadata
    item.parameters['exiftool'] = JSON.parse(`exiftool -json #{Shellwords.escape(item.file.path)}`)
  end

  def video_file?
    video_formats_extensions = ALLOWED_VIDEO_FORMATS.flat_map { |_, format| format[:extensions].each { |extension| extension } }
    file_ext = File.extname(folder_infos[:fileName]).gsub('.', '')
    video_formats_extensions.include?(file_ext)
  end

  def audio_file?
    audio_formats_extensions = ALLOWED_AUDIO_FORMATS.flat_map { |_, format| format[:extensions].each { |extension| extension } }
    file_ext = File.extname(folder_infos[:fileName]).gsub('.', '')
    audio_formats_extensions.include?(file_ext)
  end

  def text_file?
    text_formats_extensions  = ALLOWED_TEXT_FORMATS.flat_map  { |_, format| format[:extensions].each { |extension| extension } }
    file_ext = File.extname(folder_infos[:fileName]).gsub('.', '')
    text_formats_extensions.include?(file_ext)
  end

  def image_file?
    image_formats_extensions = ALLOWED_IMAGE_FORMATS.flat_map { |_, format| format[:extensions].each { |extension| extension } }
    file_ext = File.extname(folder_infos[:fileName]).gsub('.', '')
    image_formats_extensions.include?(file_ext)
  end

  def pdf_file?
    pdf_formats_extensions   = ALLOWED_PDF_FORMATS.flat_map   { |_, format| format[:extensions].each { |extension| extension } }
    file_ext = File.extname(folder_infos[:fileName]).gsub('.', '')
    pdf_formats_extensions.include?(file_ext)
  end

  def create_text_file
    if existing_text_file = folder.texts.detect { |text_file|
        text_file.file_name == folder_infos[:fileName]
      }
      existing_text_file.update!(
        :file => File.open(file.path),
        :file_name => folder_infos[:fileName],
      )
      existing_text_file
    else
      folder.texts.create(
        :file => File.open(file.path),
        :file_name => folder_infos[:fileName],
        :file_created_at => params['createdAt'],
        :file_updated_at => params['updatedAt'],
        :upload_uuid => params['uuid'],
      )
    end
  end

  def create_image_file
    if existing_image_file = folder.images.detect { |image_file|
        image_file.file_name == folder_infos[:fileName]
      }
      existing_image_file.update!(
        :file => File.open(file.path),
        :file_name => folder_infos[:fileName],
      )
      existing_image_file
    else
      folder.images.create(
        :file => File.open(file.path),
        :file_name => folder_infos[:fileName],
        :file_created_at => params['createdAt'],
        :file_updated_at => params['updatedAt'],
        :upload_uuid => params['uuid'],
      )
    end
  end

  def create_audio_file
    if existing_audio_file = folder.audio_files.detect { |audio_file|
        audio_file.file_name == folder_infos[:fileName]
      }
      existing_audio_file.update!(
        :file => File.open(file.path),
        :file_name => folder_infos[:fileName],
      )
      existing_audio_file
    else
      folder.audio_files.create(
        :file => File.open(file.path),
        :file_name => folder_infos[:fileName],
        :file_created_at => params['createdAt'],
        :file_updated_at => params['updatedAt'],
        :upload_uuid => params['uuid'],
      )
    end
  end

  def create_video_file
    if existing_video_file = folder.video_files.detect { |video_file|
        video_file.file_name == folder_infos[:fileName]
      }
      existing_video_file.update!(
        :file => File.open(file.path),
        :file_name => folder_infos[:fileName],
      )
      existing_video_file
    else
      folder.video_files.create(
        :file => File.open(file.path),
        :file_name => folder_infos[:fileName],
        :file_created_at => params['createdAt'],
        :file_updated_at => params['updatedAt'],
        :upload_uuid => params['uuid'],
      )
    end
  end

  def create_pdf_file
    if existing_pdf_file = folder.pdfs.detect { |pdf_file|
        pdf_file.file_name == folder_infos[:fileName]
      }
      existing_pdf_file.update!(
        :file => File.open(file.path),
        :file_name => folder_infos[:fileName],
      )
      existing_pdf_file
    else
      folder.pdfs.create(
        :file => File.open(file.path),
        :file_name => folder_infos[:fileName],
        :file_created_at => params['createdAt'],
        :file_updated_at => params['updatedAt'],
        :upload_uuid => params['uuid'],
      )
    end
  end

  def strip_text(text_content)
    text_content.force_encoding('Windows-1252') \
                .encode('UTF-8') \
                .gsub("\n", "") \
                .strip
  end

  def set_format_info
    file_infos = `file -b #{Shellwords.escape(file.path)}`
    item.parameters['formatInfo'] = strip_text(file_infos)
  rescue Encoding::UndefinedConversionError => exception
    Rails.logger.error(exception)
  end

  def create_folder
    hash_conditions = {
      :name => (folder_infos[:folder] || folder_infos[:rootFolder]),
      :folder => folder_infos[:rootFolder],
      :upload_uuid => params['uuid'],
    }

    if folder_infos[:subfolder]
      hash_conditions.merge!(
        :subfolder => folder_infos[:subfolder],
      )
    end

    @folder = Folder.find_by(:name => hash_conditions[:name])

    begin
      unless @folder
        @folder = Folder.create(hash_conditions)
      end
    rescue ActiveRecord::RecordNotUnique => exception
      @folder = Folder.find_by(:name => hash_conditions[:name])
    rescue => exception
      Rails.logger.error(exception)
    end
  end

  def set_folder_infos
    filePath = params['filePath'].split('/')
    fileName = filePath[filePath.length-1]

    case params['source']
    when 'folder'
      indexFileName = filePath.index(fileName)
      filePath     -= [fileName]
      rootFolder    = filePath[indexFileName-2]
      folderName    = filePath[indexFileName-1]
    when 'subfolder'
      indexFileName = filePath.index(fileName)
      filePath     -= [fileName]
      rootFolder    = filePath[indexFileName-3]
      subfolderName = filePath[indexFileName-2]
      folderName    = filePath[indexFileName-1]
    when 'root'
      indexFileName = filePath.index(fileName)
      filePath     -= [fileName]
      rootFolder    = filePath[indexFileName-1]
    end

    @folder_infos = {
      :rootFolder => rootFolder,
      :fileName => fileName,
    }

    folder_infos.merge! :folder => folderName if folderName
    folder_infos.merge! :subfolder => subfolderName if subfolderName
  end
end
