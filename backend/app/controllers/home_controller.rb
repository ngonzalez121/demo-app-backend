class HomeController < ApplicationController
  def index
    render json: {}.to_json,
      layout: false,
      status: 200
  end
end
