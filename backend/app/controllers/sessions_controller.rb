class SessionsController < ApplicationController
  include ::UserSession

  def create
    respond_to do |format|
      if user
        format.json {
          render(:show, :status => :ok)
        }
      else
        format.json {
          head(:unprocessable_entity)
        }
      end
    end
  end

  def destroy
    respond_to do |format|
      format.json { 
        render(:json => { :message => "Session terminated." }.to_json,
               :status => :ok)
      }
    end
  end
end
