class RegistrationsController < ApplicationController
  include ::UserRegistration

  def create
    respond_to do |format|
      if user_created?
        format.json { render :show, status: :ok }
      else
        format.json { render :show, status: :unprocessable_entity }
      end
    end
  end
end
