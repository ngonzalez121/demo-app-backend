class PasswordsController < ApplicationController
  include ::UserPassword

  def create
    respond_to do |format|
      if user
        format.json { 
          render(:json => { :message => "Email sent to %s" % user.email_address }.to_json,
                 :status => :ok)
        }
      else
        format.json {
          render(:json => { :message => "User not found." }.to_json,
                 :status => :unprocessable_entity)
        }
      end
    end
  end
end
