module UserRegistration
  extend ActiveSupport::Concern

  attr_accessor :user

  included do
    allow_unauthenticated_access

    before_action :create_user, only: [:create]
    before_action :start_session, only: [:create], if: -> { user.persisted? }
  end

  private

  def user_created?
    user.try(:persisted?)
  end

  def start_session
    start_new_session_for(user)
  end

  def create_user
    @user = User.create(
      :first_name => params['firstName'],
      :last_name => params['lastName'],
      :email_address => params['emailAddress'],
      :password => params['password'],
      :uuid => params['uuid'],
    )
  rescue ActiveRecord::RecordNotUnique => exception
    Rails.logger.error(exception)
  rescue StandardError => exception
    Rails.logger.error(exception)
  end
end
