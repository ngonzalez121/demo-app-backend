module UserPassword
  extend ActiveSupport::Concern

  attr_accessor :user

  included do
    allow_unauthenticated_access only: %i[create]

    before_action :set_user_by_email_address, only: %i[create]
    before_action :set_user_by_token, only: %i[edit update]
    before_action :deliver_email, only: %i[create], if: -> { user }
    before_action :update_user, only: %i[update], if: -> { user }
  end

  private

  def deliver_email
    # PasswordsMailer.reset(user).deliver_later
  end

  def set_user_by_email_address
    @user = User.find_by(email_address: params['emailAddress'])
  end

  def set_user_by_token
    @user = User.find_by_password_reset_token!(params['token'])
  end

  def update_user
    user.update(params.permit(:password, :password_confirmation))
  end

end
