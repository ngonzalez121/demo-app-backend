module UserSession
  extend ActiveSupport::Concern

  attr_accessor :user

  included do
    allow_unauthenticated_access only: %i[create]

    before_action :authenticate_user, only: %i[create]
    before_action :start_session, only: %i[create], if: -> { user }
    before_action :destroy_session, only: %i[destroy]
    before_action :clear_site_data, only: %i[destroy]

    rate_limit to: 10, within: 3.minutes, only: %i[create],
      with: -> {
        render(:json => { :message => "Try again later." }.to_json)
      }
  end

  private


  def authenticate_user
    @user = User.authenticate_by(
      :email_address => params['emailAddress'],
      :password => params['password'],
    )
  rescue => exception
    Rails.logger.error(exception)
  end

  def start_session
    start_new_session_for(user)
  end

  def clear_site_data
    # https://github.com/rails/rails/pull/54230
    response.headers["Clear-Site-Data"] = '"cache","storage"'
  end

  def destroy_session
    terminate_session
  end
end
