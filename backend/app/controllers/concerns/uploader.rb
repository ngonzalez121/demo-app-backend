module Uploader
  extend ActiveSupport::Concern

  attr_accessor :ids
  attr_accessor :upload_uuids
  attr_accessor :upload

  included do
    before_action :create_upload, only: %i[create]
    before_action :create_item, only: %i[create]
    before_action :set_uploads, only: %i[index]
  end

  private

  def upload_succeeded?
    upload.try(:persisted?)
  end

  def set_uploads
    @uploads = Upload.where(uploads_scope)
  end

  def create_item
    UploadWorker.perform_async(
      ActiveSupport::Gzip.decompress(request.body.read),
    )
  end

  def create_upload
    @upload = Upload.create(
      :file_path => params['filePath'],
      :mime_type => params['mimeType'],
      :file_created_at => params['createdAt'],
      :file_updated_at => params['updatedAt'],
      :uuid => params['uuid'],
      :user_id => Current.user.id,
    )
  rescue ActiveRecord::RecordNotUnique => exception
    Rails.logger.error(exception)
  end

  def uploads_scope
    scope = {}
    if Current.user
      scope.merge!(
        :user_id => Current.user.id,
      )
    end
    if params['folderIds']
      scope.merge!(
        :uuid => upload_uuids,
      )
    end
    scope
  end

  def decode_folder_ids
    ids = Base64.decode64(params['folderIds'])
    ids = ids.split(',') - ['']
    ids
  end

  def upload_uuids
    @ids ||= decode_folder_ids
    @upload_uuids ||= [Image, AudioFile, VideoFile, Pdf, Text].inject([]) do |array, class_name|
      array += class_name.where(:folder_id => ids).map(&:upload_uuid).uniq
      array
    end
  end
end
