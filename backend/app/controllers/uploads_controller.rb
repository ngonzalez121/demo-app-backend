class UploadsController < ApplicationController
  include Uploader

  def index
    respond_to do |format|
      format.json {
        render(
          :index,
          :status => :ok, # 200 Ok
        )
      }
    end
  end

  def create
    respond_to do |format|
      if upload_succeeded?
        format.json {
          render(
            :show,
            :status => :ok, # 200 Ok
          )
        }
      else
        format.json {
          render(
            :json => { :message => I18n.t("uploads.fail") }.to_json,
            :status => :unprocessable_entity, # 422 Unprocessable Entity
          )
        }
      end
    end
  end
end
