module Mutations
  class ShowItem < Mutations::BaseMutation
    graphql_name "showItem"

    argument :id, String, required: true

    field :folder, Types::FolderType, null: false
    field :audioFiles, [Types::AudioFileType], null: false
    field :videoFiles, [Types::VideoFileType], null: false
    field :imageFiles, [Types::ImageType], null: false
    field :pdfFiles, [Types::PdfType], null: false
    field :textFiles, [Types::TextType], null: false

    def resolve(args)
      folder = Folder.find_by(data_url: args[:id])

      MutationResult.call(
        obj: {
          folder: folder,
          audioFiles: folder.try(:audio_files),
          videoFiles: folder.try(:video_files),
          imageFiles: folder.try(:images),
          pdfFiles: folder.try(:pdfs),
          textFiles: folder.try(:texts),
        },
      )
    end
  end
end
