module Mutations
  class GetAudioFile < Mutations::BaseMutation
    graphql_name "getAudioFile"

    argument :data_url, String, required: true

    field :audio_file, Types::AudioFileType, null: false

    def resolve(args)
      audio_file = AudioFile.find_by(data_url: args[:data_url])

      MutationResult.call(
        obj: {
          audio_file: audio_file,
        },
      )
    end
  end
end
