module Mutations
  class GetVideoFile < Mutations::BaseMutation
    graphql_name "getVideoFile"

    argument :data_url, String, required: true

    field :video_file, Types::VideoFileType, null: false

    def resolve(args)
      video_file = VideoFile.find_by(data_url: args[:data_url])

      MutationResult.call(
        obj: {
          video_file: video_file,
        },
      )
    end
  end
end
