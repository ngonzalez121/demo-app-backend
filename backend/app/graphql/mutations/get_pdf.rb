module Mutations
  class GetPdf < Mutations::BaseMutation
    graphql_name "getPdf"

    argument :data_url, String, required: true

    field :pdf, Types::PdfType, null: false

    def resolve(args)
      pdf = Pdf.find_by(data_url: args[:data_url])

      MutationResult.call(
        obj: {
          pdf: pdf,
        },
      )
    end
  end
end
