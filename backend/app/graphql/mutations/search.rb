module Mutations
  class Search < Mutations::BaseMutation
    graphql_name "Search"

    argument :q, String, required: true
    argument :page, Integer, required: true
    argument :folder, String, required: false
    argument :subfolder, String, required: false

    field :results, [Types::FolderType], null: false
    field :current_page, Integer, null: false
    field :total_pages, Integer, null: false

    def resolve(args)
      folders = Folder.order("created_at::timestamp DESC") \
                      .where("data_url IS NOT NULL")

      hash_conditions = {}

      if args[:folder].present?
        hash_conditions.merge!(
          :folder => args[:folder],
        )
      end

      if args[:subfolder].present?
        hash_conditions.merge!(
          :subfolder => args[:subfolder],
        )
      end

      folders = folders.where(hash_conditions)

      if args[:q].present?
        q = args[:q].strip rescue nil
        folders = folders.search(q)
      end

      paginated_results = Kaminari.paginate_array(folders)
                                  .page(args[:page])
                                  .per(5)

      MutationResultPaginated.call(
        obj: {
          results: paginated_results,
        },
        current_page: paginated_results.current_page,
        total_pages: paginated_results.total_pages,
      )
    end
  end
end
