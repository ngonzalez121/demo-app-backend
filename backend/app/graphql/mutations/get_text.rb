module Mutations
  class GetText < Mutations::BaseMutation
    graphql_name "getText"

    argument :data_url, String, required: true

    field :text, Types::TextType, null: false

    def resolve(args)
      text = Text.find_by(data_url: args[:data_url])

      MutationResult.call(
        obj: {
          text: text,
        },
      )
    end
  end
end
