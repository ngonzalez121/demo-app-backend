module Mutations
  class GetFolder < Mutations::BaseMutation
    graphql_name "getFolder"

    argument :data_url, String, required: true

    field :folder, Types::FolderType, null: false

    def resolve(args)
      folder = Folder.find_by(data_url: args[:data_url])

      MutationResult.call(
        obj: {
          folder: folder,
        },
      )
    end
  end
end
