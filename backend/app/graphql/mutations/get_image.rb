module Mutations
  class GetImage < Mutations::BaseMutation
    graphql_name "getImage"

    argument :data_url, String, required: true

    field :image, Types::ImageType, null: false

    def resolve(args)
      image = Image.find_by(data_url: args[:data_url])

      MutationResult.call(
        obj: {
          image: image,
        },
      )
    end
  end
end
