module Types
  class MutationType < Types::BaseObject
    field :search, mutation: Mutations::Search
    field :show_item, mutation: Mutations::ShowItem
    field :get_folder, mutation: Mutations::GetFolder
    field :get_pdf, mutation: Mutations::GetPdf
    field :get_image, mutation: Mutations::GetImage
    field :get_audio_file, mutation: Mutations::GetAudioFile
    field :get_video_file, mutation: Mutations::GetVideoFile
    field :get_text, mutation: Mutations::GetText
  end
end
