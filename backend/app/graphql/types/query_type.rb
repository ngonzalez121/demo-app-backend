module Types
  class QueryType < Types::BaseObject
    field :folders, [Types::FolderType], null: false
    def folders
      Folder.limit(100).all
    end

    field :folder, Types::FolderType, null: false do
      argument :id, ID, required: true
    end
    def folder(id:)
      Folder.find(id)
    end
  end
end
