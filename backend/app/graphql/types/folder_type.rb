module Types
  class FolderType < Types::BaseObject
    graphql_name "Folder"

    implements GraphQL::Types::Relay::Node

    global_id_field :id

    field :attributes, GraphQL::Types::JSON, null: false

    def attributes
      object.to_builder.attributes!
    end
  end
end
