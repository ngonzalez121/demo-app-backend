##### Application Development Environment

| Name                      | Type   | Description                    |
|:--------------------------|:-------|:-------------------------------|
| `APP_HOSTNAME`            | String | Application Hostname           |
| `APP_PORT`                | Int    | Application Port               |
| `NGINX_PORT`              | Int    | Nginx Port                     |
| `AZURE_ACCESS_KEY`        | String | Azure Storage Access Key       |
| `AZURE_ACCOUNT_NAME`      | String | Azure Storage Account Name     |
| `AZURE_CONTAINER_NAME`    | String | Azure Storage Container Name   |
| `POSTGRESQL_HOST`         | String | PostgreSQL Hostname            |
| `POSTGRESQL_PORT`         | String | PostgreSQL Port                |
| `POSTGRESQL_USERNAME`     | String | PostgreSQL Username            |
| `POSTGRESQL_PASSWORD`     | String | PostgreSQL Password            |
| `POSTGRESQL_DB`           | String | PostgreSQL Database            |
| `REDIS_HOST`              | String | Redis Hostname                 |
| `REDIS_PORT`              | Int    | Redis Port                     |
| `REDIS_DB`                | Int    | Redis Database                 |
| `SECRET_KEY_BASE`         | String | Rails Secret Key               |

##### GitLab CI/CD variables

| Name                      | Type   | Description                    |
|:--------------------------|:-------|:-------------------------------|
| `CI_DEPLOY_PASSWORD`      | String | GitLab Deploy Token            |
| `CI_DEPLOY_USER`          | String | GitLab Username                |
| `CLIENT_ID`               | String | Azure App Client ID            |
| `CLIENT_SECRET`           | String | Azure App Client Secret        |
| `CLUSTER_NAME`            | String | Azure Cluster Name             |
| `GITLAB_USER_EMAIL`       | String | GitLab User Email              |
| `LOAD_BALANCER_PUBLIC_IP` | String | Azure Load Balancer Public IP  |
| `NGINX_PORT`              | Int    | Nginx Port                     |
| `NGINX_STATUS_PORT`       | Int    | Nginx Status Port              |
| `RESOURCE_GROUP_NAME`     | String | Azure Resource Group Name      |
| `SUBSCRIPTION_ID`         | String | Azure Subscription ID          |
| `TENANT_ID`               | String | Azure Tenant ID                |

###### Docker: Build Application

```shell
docker build . -f .gitlab/docker/app/Dockerfile \
  -t app-backend
```

###### Search: GraphQL Mutation

![logo](https://i.ibb.co/4Fw0VhH/Screenshot-from-2024-03-30-23-13-24.png)

```gql
  mutation search($q: String!, $page: Int!, $folder: String, $subfolder: String) {
    search(input: { q: $q, page: $page, folder: $folder, subfolder: $subfolder }) {
      results {
        attributes
      }
      loading
      errors
      currentPage
      totalPages
    }
  }
```
